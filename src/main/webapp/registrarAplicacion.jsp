<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.autenticador.dominio.*"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	String p=request.getParameter("p");
	JSONObject aplicacion=new JSONObject(p);
	
	JSONObject respuesta=new JSONObject();
	try {
		respuesta=AutenticacionManager.get().registrar(aplicacion);		
		respuesta.put("result", "OK");
	}
	catch (Exception e) {
		respuesta.put("result", "ERROR");
		respuesta.put("mensaje", e.getMessage());
	}
	out.println(respuesta.toString());
%>







