<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.autenticador.dominio.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Autenticador</title>
</head>
<body>
	<h1>Autenticador propio - autenticar.jsp</h1><br>
	
	<i>Necesitas un servidor de mongoDB corriendo en local</i>
	
<%
	long valorDelToken=Long.parseLong(request.getParameter("token"));
	TokenDeIdentificacion token=AutenticacionManager.get().getToken(valorDelToken);
	if (token==null) {
		out.print("Token inválido");
	} else {
		String callback=token.getCallback();
%>
	La aplicación <%= token.getAplicacion() %> quiere guardar mensajes en tu cuenta.<br>
	¿Aceptas?<br>
	<a href="aceptar.jsp?token=<%= valorDelToken %>">Sí</a>   <a href="<%= callback %>">No</a>
	
<%		
	}
%>


</body>
</html>