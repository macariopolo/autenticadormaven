<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.autenticador.dominio.*"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	String p=request.getParameter("p");
	JSONObject keys=new JSONObject(p);
	
	JSONObject respuesta=new JSONObject();
	try {
		long valorDelToken=AutenticacionManager.get().crearToken(keys);
		
		String url="http://localhost:8090/Autenticador/autenticar.jsp?token=" + valorDelToken + "&consumerKey=" + keys.getString(Ctes.consumerKey);
		response.addHeader("urlDeAutenticacion", url);
	}
	catch (Exception e) {
		respuesta.put("result", "ERROR");
		respuesta.put("mensaje", e.getMessage());
	}
	out.println(respuesta.toString());
%>







