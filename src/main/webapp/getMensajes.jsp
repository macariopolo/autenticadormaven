<%@page import="org.json.*, edu.uclm.esi.tysweb.autenticador.dominio.*"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	JSONObject respuesta=new JSONObject();
	try {
		Object oEmail=session.getAttribute("email");
		if (oEmail==null)
			throw new Exception("Debes estar logueado para enviar mensajes");
		JSONArray mensajes=AutenticacionManager.get().getMensajes(oEmail.toString());
		respuesta.put("mensajes", mensajes);
		respuesta.put("result", "OK");
	}
	catch (Exception e) {
		respuesta.put("result", "ERROR");
		respuesta.put("mensaje", e.getMessage());
	}
	out.println(respuesta.toString());
%>
