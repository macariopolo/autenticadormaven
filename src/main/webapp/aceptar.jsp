<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.autenticador.dominio.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Autenticador</title>
</head>
<body>
	<h1>Autenticador propio - aceptar.jsp</h1>
	
	<i>Necesitas un servidor de mongoDB corriendo en local</i>

<%
	long valorDelToken=Long.parseLong(request.getParameter("token"));
	TokenDeIdentificacion token=AutenticacionManager.get().getToken(valorDelToken);
	if (token==null) {
		out.print("Token inválido");
		return;
	} else {
		String callback=token.getCallback();
	}
%>
	<h2>Escribe tus credenciales para que <%= token.getAplicacion() %> pueda guardar mensajes en tu cuenta</h2>
	<form method="post" action="validarCredencialesConToken.jsp">
		<input type="text" placeholder="Nombre de usuario" name="email"><br>
		<input type="password" placeholder="Contraseña" name="pwd"><br>
		<input type="hidden" value="<%=valorDelToken%>" name="token">
		<button type="submit">Aceptar</button>
	</form>
</body>
</html>