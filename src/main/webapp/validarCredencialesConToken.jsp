<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.autenticador.dominio.*"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	String email=request.getParameter("email");
	String pwd=request.getParameter("pwd");
	long valorDelToken=Long.parseLong(request.getParameter("token"));
	
	JSONObject respuesta=new JSONObject();
	try {
		TokenDeRecurso tokenDeRecurso=AutenticacionManager.get().validarTokenDeUsuario(email, pwd, valorDelToken);
		String url=tokenDeRecurso.getCallback() + "?tokenDeRecurso=" + tokenDeRecurso.getValor();
		response.sendRedirect(url);
	}
	catch (Exception e) {
		respuesta.put("result", "ERROR");
		respuesta.put("mensaje", e.getMessage());
	}
	out.println(respuesta.toString());
%>
