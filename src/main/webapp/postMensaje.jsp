<%@page import="org.json.JSONObject, edu.uclm.esi.tysweb.autenticador.dominio.*"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	
	JSONObject respuesta=new JSONObject();
	try {
		String email=session.getAttribute("email").toString();
		if (email==null)
			throw new Exception("Debes estar logueado para enviar mensajes");
		String mensaje=request.getParameter("mensaje");
		AutenticacionManager.get().post(email, mensaje);
		respuesta.put("result", "OK");
	}
	catch (Exception e) {
		respuesta.put("result", "ERROR");
		respuesta.put("mensaje", e.getMessage());
	}
	out.println(respuesta.toString());
%>
