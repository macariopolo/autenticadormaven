package edu.uclm.esi.tysweb.autenticador.dominio;

import java.util.Random;

public class TokenDeRecurso {
	private long valor;
	private String callback;
	private long caducidad;
	private String aplicacion;
	private boolean usado;
	private String usuario;

	public TokenDeRecurso(String callback, String aplicacion, String usuario) {
		this.valor=new Random().nextLong();
		if (this.valor<0)
			this.valor=-this.valor;
		this.callback=callback;
		this.aplicacion=aplicacion;
		this.usuario=usuario;
		this.caducidad=System.currentTimeMillis() + 60*5*1000; // 5 minutos
	}

	public long getValor() {
		return this.valor;
	}

	public long getCaducidad() {
		return this.caducidad;
	}

	public String getAplicacion() {
		return aplicacion;
	}
	
	public String getCallback() {
		return callback;
	}

	public void setUsado(boolean usado) {
		this.usado=true;
	}
	
	public boolean getUsado() {
		return this.usado;
	}
	
	public String getUsuario() {
		return this.usuario;
	}
}
