package edu.uclm.esi.tysweb.autenticador.dominio;

import java.util.Hashtable;

import org.bson.BsonDocument;
import org.bson.BsonString;
import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.tysweb.autenticador.dao.AutenticacionMongoBroker;

public class AutenticacionManager {
	private Hashtable<Long, TokenDeIdentificacion> tokensDeIdentificacion;
	private Hashtable<Long, TokenDeRecurso> tokensDeRecurso;

	private AutenticacionManager() {
		this.tokensDeIdentificacion=new Hashtable<>();
		this.tokensDeRecurso=new Hashtable<>();
	}
	
	public JSONObject registrar(JSONObject aplicacion) throws Exception {
		BsonDocument bso=new BsonDocument();
		bso.append(Ctes.aplicacion, new BsonString(aplicacion.getString("nombre")));
		bso.append(Ctes.callback, new BsonString(aplicacion.getString("callback")));
		bso=AutenticacionMongoBroker.get().insertAplicacion(bso);
		JSONObject result=new JSONObject();
		result.put(Ctes.consumerKey, bso.getString(Ctes.consumerKey).getValue());
		result.put(Ctes.consumerSecret, bso.getString(Ctes.consumerSecret).getValue());
		return result;
	}
	
	public long crearToken(JSONObject keys) throws Exception {
		BsonDocument bso=new BsonDocument();
		String consumerKey=keys.getString(Ctes.consumerKey);
		String consumerSecret=keys.getString(Ctes.consumerSecret);
		bso.append(Ctes.consumerKey, new BsonString(consumerKey));
		bso.append(Ctes.consumerSecret, new BsonString(consumerSecret));
		
		JSONObject callbackYAplicacion=AutenticacionMongoBroker.get().findKeys(bso);
		String callback=callbackYAplicacion.getString(Ctes.callback);
		String nombreAplicacion=callbackYAplicacion.getString(Ctes.aplicacion);
		TokenDeIdentificacion token=new TokenDeIdentificacion(callback, nombreAplicacion, consumerKey);
		this.tokensDeIdentificacion.put(token.getValor(), token);
		return token.getValor();
	}
	
	public TokenDeIdentificacion getToken(long valorDelToken) {
		TokenDeIdentificacion token=this.tokensDeIdentificacion.get(valorDelToken);
		if (token==null)
			return null;
		if (token.getUsado())
			return null;
		long horaActual=System.currentTimeMillis();
		if (horaActual>token.getCaducidad())
			return null;
		return token;
	}
	
	public void identificar(String email, String pwd) throws Exception {
		AutenticacionMongoBroker.get().identificar(email, pwd);
	}
	
	public TokenDeRecurso validarTokenDeUsuario(String email, String pwd, long valorDelToken) throws Exception {
		AutenticacionMongoBroker.get().identificar(email, pwd);
		TokenDeIdentificacion token=this.getToken(valorDelToken);
		if (token==null)
			throw new Exception("Token inválido");
		TokenDeRecurso tokenDeRecurso=new TokenDeRecurso(token.getCallback(), token.getAplicacion(), email);
		this.tokensDeRecurso.put(tokenDeRecurso.getValor(), tokenDeRecurso);
		return tokenDeRecurso;
	}
	
	public void crearUsuario(String email, String pwd) throws Exception {
		AutenticacionMongoBroker.get().crearUsuario(email, pwd);
	}
	
	public void post(String email, String mensaje) throws Exception {
		AutenticacionMongoBroker.get().post(email, mensaje);
	}
	
	public void post(long valorTokenDeRecurso, String mensaje) throws Exception {
		TokenDeRecurso token=this.tokensDeRecurso.get(valorTokenDeRecurso);
		AutenticacionMongoBroker.get().post(token.getUsuario(), mensaje);
	}
	
	public JSONArray getMensajes(String email) throws Exception {
		return AutenticacionMongoBroker.get().getMensajes(email);
	}
	
	private static class ManagerHolder {
		static AutenticacionManager singleton=new AutenticacionManager();
	}
	
	public static AutenticacionManager get() {
		return ManagerHolder.singleton;
	}
}








