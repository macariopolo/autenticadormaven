package edu.uclm.esi.tysweb.autenticador.dominio;

import java.util.Random;

public class TokenDeIdentificacion {
	private long valor;
	private String callback;
	private long caducidad;
	private String aplicacion;
	private boolean usado;
	private String consumerKey;

	public TokenDeIdentificacion(String callback, String aplicacion, String consumerKey) {
		this.valor=new Random().nextLong();
		if (this.valor<0)
			this.valor=-this.valor;
		this.callback=callback;
		this.aplicacion=aplicacion;
		this.consumerKey=consumerKey;
		this.caducidad=System.currentTimeMillis() + 60*5*1000; // 5 minutos
	}

	public long getValor() {
		return this.valor;
	}

	public long getCaducidad() {
		return this.caducidad;
	}

	public String getAplicacion() {
		return aplicacion;
	}
	
	public String getCallback() {
		return callback;
	}

	public void setUsado(boolean usado) {
		this.usado=true;
	}
	
	public boolean getUsado() {
		return this.usado;
	}
	
	public String getConsumerKey() {
		return consumerKey;
	}
}
