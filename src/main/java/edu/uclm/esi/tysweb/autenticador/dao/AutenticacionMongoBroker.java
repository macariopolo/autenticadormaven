package edu.uclm.esi.tysweb.autenticador.dao;

import java.security.MessageDigest;
import java.util.Random;

import org.bson.BsonDocument;
import org.bson.BsonObjectId;
import org.bson.BsonString;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;

import edu.uclm.esi.tysweb.autenticador.dominio.Ctes;

public class AutenticacionMongoBroker {
	private MongoClient mongoClient;
	
	private AutenticacionMongoBroker() {
		String uri="mongodb://tysweb:tysweb20182019@ds159782.mlab.com:59782/juegos";
		MongoClientURI clientUri=new MongoClientURI(uri);
		this.mongoClient=new MongoClient(clientUri);
	}
		
	private static class MongoBrokerHolder {
		static AutenticacionMongoBroker singleton=new AutenticacionMongoBroker();
	}
	
	public static AutenticacionMongoBroker get() {
		return MongoBrokerHolder.singleton;
	}

	public BsonDocument insertAplicacion(BsonDocument bso) {
		long n=new Random().nextLong();
		if (n<0) 
			n=-n;
		bso.append(Ctes.consumerKey, new BsonString(""+n));
		n=new Random().nextLong();
		if (n<0) 
			n=-n;
		bso.append(Ctes.consumerSecret, new BsonString(""+n));
		mongoClient.getDatabase("juegos").getCollection("aplicaciones", BsonDocument.class).insertOne(bso);
		return bso;
	}

	public JSONObject findKeys(BsonDocument keys) throws Exception {
		BsonDocument aplicacion = mongoClient.getDatabase("juegos").getCollection("aplicaciones", BsonDocument.class).find(keys).first();
		if (aplicacion==null)
			throw new Exception("Error de identificación");
		
		String callback=aplicacion.getString(Ctes.callback).getValue();
		String nombreAplicacion=aplicacion.getString(Ctes.aplicacion).getValue();
		JSONObject result=new JSONObject();
		result.put(Ctes.callback, callback);
		result.put(Ctes.aplicacion, nombreAplicacion);
		return result;
	}

	public void identificar(String email, String pwd) throws Exception {
		BsonDocument criterio=new BsonDocument();
		criterio.append("email", new BsonString(email));
		criterio.append("pwd", new BsonString(hash(pwd)));
		BsonDocument usuario=mongoClient.getDatabase("juegos").getCollection("usuarios", BsonDocument.class).find(criterio).first();
		if (usuario==null)
			throw new Exception("Credenciales inválidas");
	}
	
	public void crearUsuario(String email, String pwd) {
		BsonDocument usuario=new BsonDocument();
		usuario.append("email", new BsonString(email));
		usuario.append("pwd", new BsonString(hash(pwd)));
		mongoClient.getDatabase("juegos").getCollection("usuarios", BsonDocument.class).insertOne(usuario);
	}
	
	public void post(String email, String texto) {
		BsonDocument criterio=new BsonDocument();
		criterio.append("email", new BsonString(email));
		BsonDocument usuario=mongoClient.getDatabase("juegos").getCollection("usuarios", BsonDocument.class).find(criterio).first();
		BsonObjectId id = usuario.getObjectId("_id");
		
		BsonDocument mensaje=new BsonDocument();
		mensaje.append("usuario_id", id);
		mensaje.append("texto", new BsonString(texto));
		mongoClient.getDatabase("juegos").getCollection("mensajes", BsonDocument.class).insertOne(mensaje);
	}
	
	public JSONArray getMensajes(String email) {
		BsonDocument criterio=new BsonDocument();
		criterio.append("email", new BsonString(email));
		BsonDocument usuario=mongoClient.getDatabase("juegos").getCollection("usuarios", BsonDocument.class).find(criterio).first();
		BsonObjectId id = usuario.getObjectId("_id");
		
		criterio=new BsonDocument();
		criterio.append("usuario_id", id);
		FindIterable<BsonDocument> mensajes = mongoClient.getDatabase("juegos").getCollection("mensajes", BsonDocument.class).find(criterio);
		JSONArray result=new JSONArray();
		MongoCursor<BsonDocument> itMensajes = mensajes.iterator();
		BsonDocument mensaje;
		while (itMensajes.hasNext()) {
			mensaje=itMensajes.next();
			result.put(mensaje.getString("texto").getValue());
		}
		return result;
	}

	private String hash(String pwd) {
		try {
			byte[] bytesOfMessage = pwd.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] result = md.digest(bytesOfMessage);
			return new String(result);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}